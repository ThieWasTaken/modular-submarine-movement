using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DesktopInputHandler : MonoBehaviour
{
    [NonReorderable] public KeysEventPair[] inputs;
    void Update()
    {
        foreach (var pair in inputs)
        {
            List<bool> triggered = new List<bool>();
            foreach (var key in pair.keyConfigSO.value.keys)
            {
                switch (key.pressStyle)
                {
                    case KeysEventSO.KeyPressingConfiguration.Once:
                        triggered.Add(Input.GetKeyDown(key.key));
                        break;
                    case KeysEventSO.KeyPressingConfiguration.Hold:
                        triggered.Add(Input.GetKey(key.key));
                        break;
                }
            }
            if (triggered.TrueForAll(x => x))
            {
                pair.action?.Invoke();
            }
        }
    }

    [System.Serializable]
    public class KeysEventPair
    {
        public KeysEventSO keyConfigSO;
        public UnityEvent action;
    }
}
