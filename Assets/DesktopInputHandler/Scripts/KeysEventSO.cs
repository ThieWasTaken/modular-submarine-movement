using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "new KeyEventPair", menuName = "DesktopInputs/KeyEventPair")]
public class KeysEventSO : ScriptableObject
{
    public KeysEvent value;
    private void OnEnable()
    {
        var cached = PlayerPrefs.GetString(name, null);
        if (!string.IsNullOrEmpty(cached))
        {
            value = JsonUtility.FromJson<KeysEvent>(cached);
        }
    }

    [System.Serializable]
    public class KeysEvent
    {
        public KeyConfiguration[] keys;
    }

    [System.Serializable]
    public class KeyConfiguration
    {
        public KeyCode key;
        public KeyPressingConfiguration pressStyle;
    }

    public enum KeyPressingConfiguration
    {
        Once,
        Hold
    }
}
