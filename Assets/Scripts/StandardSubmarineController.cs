using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardSubmarineController : ASubmarineController
{
    public Animator anim;
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        anim.SetFloat("Speed", rb.velocity.sqrMagnitude / new Vector3(maxDrifitingMovementSpeed, maxLinearSurfingMovementSpeed, maxLinearMovementSpeed).sqrMagnitude);
    }
}
