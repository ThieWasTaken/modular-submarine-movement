﻿using System.Collections;
using UnityEngine;

public abstract class ASubmarineController : MonoBehaviour
{
    public Rigidbody rb;
    #region Linear Movement
    private Vector3 linearVelocity;
    private Coroutine brakeLinearlyCO;
    public float maxLinearMovementSpeed;
    public float maxDrifitingMovementSpeed;
    public float maxLinearDivingMovementSpeed;
    public float maxLinearSurfingMovementSpeed;
    public void ChangeZLinearVelocity(float deltaVelocity)
    {
        linearVelocity.x = Mathf.Clamp(linearVelocity.x + deltaVelocity, -maxLinearMovementSpeed, maxLinearMovementSpeed);
    }
    public void ChangeYLinearVelocity(float deltaVelocity)
    {
        linearVelocity.y = Mathf.Clamp(linearVelocity.y + deltaVelocity, -maxLinearDivingMovementSpeed, maxLinearSurfingMovementSpeed);
    }
    public void ChangeXLinearVelocity(float deltaVelocity)
    {
        linearVelocity.z = Mathf.Clamp(linearVelocity.z + deltaVelocity, -maxDrifitingMovementSpeed, maxDrifitingMovementSpeed);
    }

    public void BreakLinearlyOver(float duration)
    {
        if (brakeLinearlyCO == null) brakeLinearlyCO = StartCoroutine(BrakeOverLinearlyCoroutine(duration));
    }

    private IEnumerator BrakeOverLinearlyCoroutine(float duration)
    {
        float xDeceleration = linearVelocity.x / duration;
        float yDeceleration = linearVelocity.y / duration;
        float zDeceleration = linearVelocity.z / duration;
        while (linearVelocity.sqrMagnitude != 0)
        {
            if (linearVelocity.x != 0) linearVelocity.x = Halt(linearVelocity.x, xDeceleration * Time.deltaTime);
            if (linearVelocity.y != 0) linearVelocity.y = Halt(linearVelocity.y, yDeceleration * Time.deltaTime);
            if (linearVelocity.z != 0) linearVelocity.z = Halt(linearVelocity.z, zDeceleration * Time.deltaTime);
            yield return null;
        }
        brakeLinearlyCO = null;
    }

    #endregion LinearMovement

    #region AngularMovement
    private Coroutine brakeAngularlyCO;
    private Vector3 angularVelocity;
    public float maxRotatingSpeed;
    public float maxAngularDivingSpeed;
    public float maxAngularSurfingSpeed;

    public void ChangeZAngularVelocity(float deltaVelocity)
    {
        angularVelocity.z = Mathf.Clamp(angularVelocity.z + deltaVelocity, -maxAngularDivingSpeed, maxAngularSurfingSpeed);
    }

    public void ChangeYAngularVelocity(float deltaVelocity)
    {
        angularVelocity.y = Mathf.Clamp(angularVelocity.y + deltaVelocity, -maxRotatingSpeed, maxRotatingSpeed);
    }

    public void BrakeAngularlyOver(float duration)
    {
        if (brakeAngularlyCO == null) brakeAngularlyCO = StartCoroutine(BrakeAngularlyOverCoroutine(duration));
    }

    private IEnumerator BrakeAngularlyOverCoroutine(float duration)
    {
        Debug.Log("braking angularly");
        float zDeceleration = Mathf.Abs( angularVelocity.z / duration);
        float yDeceleration = Mathf.Abs( angularVelocity.y / duration);
        while (angularVelocity.sqrMagnitude != 0)
        {
            if (angularVelocity.z != 0) angularVelocity.z = Halt(angularVelocity.z, zDeceleration * Time.deltaTime);
            if (angularVelocity.y != 0) angularVelocity.y = Halt(angularVelocity.y, yDeceleration * Time.deltaTime);
            Debug.Log(angularVelocity);
            yield return null;
        }
        brakeAngularlyCO = null;
    }
    #endregion AngularMovement


    private float Halt(float x, float deceleration)
    {
        Debug.Log(x + ", " + deceleration);
        if (Mathf.Abs(x - deceleration) < deceleration) return 0;
        if (x > 0) return x - deceleration;
        else return x + deceleration;
    }

    protected virtual void FixedUpdate()
    {
        rb.velocity = transform.TransformDirection(linearVelocity);
        var localAngularVelocity = transform.TransformDirection(angularVelocity);
        rb.angularVelocity =  new Vector3(Mathf.Deg2Rad * localAngularVelocity.x, Mathf.Deg2Rad * localAngularVelocity.y, Mathf.Deg2Rad * localAngularVelocity.z);
    }
}